//
//  FlowrSpotUITests.swift
//  FlowrSpotUITests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest

class FlowrSpotUITests: XCTestCase {

        var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
    }
    override func tearDown() {
        super.tearDown()
        app = nil
    }
    func testGoingThroughApp() {
        app.launch()
        let homeCollectionView = app.collectionViews
        XCTAssertNotEqual(homeCollectionView.cells.count, 0)

        let firstHomeFlowerCellExist =
            homeCollectionView.cells.element(boundBy:0)
        XCTAssertNotNil(firstHomeFlowerCellExist)
        firstHomeFlowerCellExist.tap()

        let detailTableView = app.tables.containing(.table, identifier: "detailTable")
        XCTAssertNotNil(detailTableView)

        let cellSighting = detailTableView.cells.containing(.cell, identifier: "FlowerSightingTableViewCell")
        XCTAssertNotNil(cellSighting)

        let sightingCellElement = cellSighting.element
        XCTAssertNotNil(sightingCellElement)

        let addNewSightButton = app.tables.buttons["+ Add New Sighting"]
        XCTAssertNotNil(addNewSightButton)
        addNewSightButton.tap()

        let flowrspotNavigationBar = app.navigationBars["FlowrSpot"]
        XCTAssertNotNil(flowrspotNavigationBar)
        flowrspotNavigationBar.buttons["backIcon"].tap()
        flowrspotNavigationBar.tap()
    }
}
