//
//  FlowersDownloader.swift
//  FlowrSpot
//
//  Created by TK on 22/01/2018.
//  Copyright © 2018 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol DownloaderActivity {
    func showLoadingSpinner()
    func hideLoadingSpinner()
}
final class FlowersDownloader {
  var container: UIView!
  var indicator: UIActivityIndicatorView!

  func fetchFlowersList(success: RestClient.SuccessCompletion<[FlowerResponse]>, failure: RestClient.FailureCompletion) {
    RestClient.shared.request(FlowerRequests.flowersList, version: .v1, success: { json in
      let flowers = FlowerResponse.parseArray(json, key: "flowers")
      success?(flowers)
    }, failure: failure)
  }
    
  func fetchFlowerDetails(flowerId: Int, success: RestClient.SuccessCompletion<FlowerDetailsResponse>, failure: RestClient.FailureCompletion) {
    RestClient.shared.request(FlowerRequests.flowerDetails(flowerid: flowerId), version: .v1, success: { (json) in
      if let flowerDetails = FlowerDetailsResponse.parse(json, key: "flower") {
        success?(flowerDetails)
      } else {
        failure?(RemoteResourceError.invalidJson)
      }
    }, failure: failure)
  }
    
func fetchSpecificFlowers(query:String, success: RestClient.SuccessCompletion<[FlowerResponse]>, failure: RestClient.FailureCompletion ){
    RestClient.shared.request(FlowerRequests.searchFlowers(query: query), version: .v1, success: { json in
        let flowers = FlowerResponse.parseArray(json, key: "flowers")
            success?(flowers)
        },  failure: failure)
    }
    
  func fetchFlowerSighting(flowerId: Int, success: RestClient.SuccessCompletion<[SightingResponse]>, failure: RestClient.FailureCompletion) {
    RestClient.shared.request(FlowerDetailRequest.flowerSighting(flowerId: flowerId), version: .v1, success: { (json) in
        let flowerSighting = SightingResponse.parseArray(json, key: "sightings")
        success?(flowerSighting)
      }, failure: failure)
   }
}
extension FlowersDownloader: DownloaderActivity {

    func showLoadingSpinner() {
        let view = UIApplication.topViewController()?.view
        guard view != nil else { return }
        
        container                       = UIView()
        container.frame                 = view!.frame
        container.center                = view!.center
        container.backgroundColor       = UIColor.clear

        let loadingView: UIView         = UIView()

        loadingView.frame               = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center              = view!.center
        loadingView.backgroundColor     = UIColor(r: 68, g: 68, b: 68).withAlphaComponent(0.7)
        loadingView.clipsToBounds       = true
        loadingView.layer.cornerRadius  = 10

        indicator       = UIActivityIndicatorView()
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.style =
            UIActivityIndicatorView.Style.whiteLarge
        indicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                    y: loadingView.frame.size.height / 2)
        loadingView.addSubview(indicator)

        let message =  "loading_activity".localized()
        let label       = UILabel(frame: CGRect(x: 0, y: 50, width: 80, height: 30))
        label.textColor = UIColor.white
        label.text      = message
        label.textAlignment = .center
        label.font      = UIFont.custom(type: .regular, size: 14.0)
        loadingView.addSubview(label)
        container.addSubview(loadingView)
        view?.addSubview(container)
        indicator.startAnimating()
    }
    func hideLoadingSpinner() {
        if  container != nil {
            indicator.stopAnimating()
            indicator = nil
            container.removeFromSuperview()
            container = nil
        }
    }
}
