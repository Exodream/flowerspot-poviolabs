//
//  Sighting.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct Sighting {
    var commentsCount: Int!
    var createdAt: String!
    var descriptionField: String!
    var flower: Flower!
    var id: Int!
    var latitude: Float!
    var likesCount: Int!
    var longitude: Float!
    var name: String!
    var picture: String!
    var user: User!

    init(fromDictionary dictionary: [String: Any]) {
        commentsCount = dictionary["comments_count"] as? Int
        createdAt = dictionary["created_at"] as? String
        descriptionField = dictionary["description"] as? String
        if let flowerData = dictionary["flower"] as? [String: Any] {
            flower = Flower(fromDictionary: flowerData)
        }
        id = dictionary["id"] as? Int
        latitude = dictionary["latitude"] as? Float
        likesCount = dictionary["likes_count"] as? Int
        longitude = dictionary["longitude"] as? Float
        name = dictionary["name"] as? String
        picture = dictionary["picture"] as? String
        if let userData = dictionary["user"] as? [String: Any] {
            user = User(fromDictionary: userData)
        }
    }
}
