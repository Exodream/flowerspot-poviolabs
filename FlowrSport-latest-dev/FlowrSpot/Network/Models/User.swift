//
//  User.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct User {
    var fullName: String!
    var id: Int!

    init(fromDictionary dictionary: [String: Any]) {
        fullName = dictionary["full_name"] as? String
        id = dictionary["id"] as? Int
    }
}
