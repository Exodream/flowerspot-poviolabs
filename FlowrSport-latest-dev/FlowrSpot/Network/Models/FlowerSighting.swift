//
//  FlowerSighting.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct FlowerSighting {
    var meta: Meta!
    var sightings: [Sighting]!
}
