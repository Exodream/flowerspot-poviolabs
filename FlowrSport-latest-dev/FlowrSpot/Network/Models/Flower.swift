//
//  Flower.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct Flower {
    var id: Int!
    var name: String!
    var latinName: String!
    var sightings: Int!
    var url: String!
    var isFavorite: Bool!
    var description: String!

    init(id: Int, name: String, latinName: String, sightings: Int, url: String, isFavorite: Bool, description: String) {
        self.id = id; self.name = name;self.latinName = latinName
        self.sightings = sightings;self.url = url;self.isFavorite = isFavorite;self.description = description
    }
    init(fromDictionary dictionary: [String: Any]) {
        id = dictionary["id"] as? Int
        latinName = dictionary["latin_name"] as? String
        name = dictionary["name"] as? String
        url = "http:" + (dictionary["profile_picture"] as? String)!
    }
    
}
