//
//  FlowerDetail.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct FlowerDetail {
    let id: Int
    let name: String
    let latinName: String
    let sightings: Int
    let url: String
    let isFavorite: Bool
    let description: String
    let features: [String]?
}
