//
//  FlowerDetailsResponse.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import ObjectMapper

class FlowerDetailsResponse: FlowerResponse {
  private(set) var features: [String]?
  
  required convenience init(map: Map) {
    self.init()
  }
    override func mapping(map: Map) {
        super.mapping(map: map)
        features    <- map["features"]
    }
}
