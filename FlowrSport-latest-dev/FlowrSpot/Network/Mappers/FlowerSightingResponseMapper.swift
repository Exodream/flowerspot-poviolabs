//
//  FlowerSightingResponseMapper.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
final class FlowerSightingResponseMapper: ResponseMapper {
   static func transform(response: [SightingResponse]) -> [Sighting]? {
    return response.compactMap { Sighting(fromDictionary: $0.toJSON())}
    }
}
