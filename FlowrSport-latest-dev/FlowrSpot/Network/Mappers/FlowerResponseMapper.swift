//
//  FlowerResponseMapper.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

final class FlowerResponseMapper: ResponseMapper {
  static func transform(response: FlowerResponse) -> Flower? {
    return Flower(id: response.id, name: response.name, latinName: response.latinName, sightings: response.sightings, url: "http:" + response.profilePicture, isFavorite: response.isFavorite, description: response.description)
  }
}
