//
//  FlowerSigntingResponse.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class FlowerSightingResponse: Mappable {

    var meta: MetaResponse?
    var sightings: [SightingResponse]?
    
    class func newInstance(map: Map) -> Mappable? {
        return FlowerSightingResponse(map: map)
    }
    required convenience init(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        meta <- map["meta"]
        sightings <- map["sightings"]
    }
}
class SightingResponse: Mappable {

    var commentsCount: Int?
    var createdAt: String?
    var descriptionField: String?
    var flower: FlowerResponse?
    var id: Int?
    var latitude: Float?
    var likesCount: Int?
    var longitude: Float?
    var name: String?
    var picture: String?
    var user: UserResponse?

    class func newInstance(map: Map) -> Mappable? {
        return SightingResponse(map: map)
    }
    required convenience init(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        commentsCount <- map["comments_count"]
        createdAt <- map["created_at"]
        descriptionField <- map["description"]
        flower <- map["flower"]
        id <- map["id"]
        latitude <- map["latitude"]
        likesCount <- map["likes_count"]
        longitude <- map["longitude"]
        name <- map["name"]
        picture <- map["picture"]
        user <- map["user"]
    }
}
class MetaResponse: Mappable {
    
    var pagination: PaginationResponse?
    
    class func newInstance(map: Map) -> Mappable? {
        return MetaResponse(map: map)
    }
    required convenience init(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        pagination <- map["pagination"]
    }
}
class UserResponse: Mappable {

    var fullName: String?
    var id: Int?

    class func newInstance(map: Map) -> Mappable? {
        return UserResponse(map: map)
    }
    required convenience init(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        fullName <- map["full_name"]
        id <- map["id"]
    }
}
class PaginationResponse: Mappable {

    var currentPage: Int?
    var nextPage: Int?
    var prevPage: Int?
    var totalPages: Int?

    class func newInstance(map: Map) -> Mappable? {
        return PaginationResponse(map: map)
    }
    required convenience init(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        currentPage <- map["current_page"]
        nextPage <- map["next_page"]
        prevPage <- map["prev_page"]
        totalPages <- map["total_pages"]
    }
}

