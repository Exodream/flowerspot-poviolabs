//
//  HomeInteractor.swift
//  FlowrSpot
//
//  Created by TK on 16/01/2018.
//  Copyright © 2018 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol HomeBusinessLogic {
  func fetchFlowers()
  func fetchFlowerDetails(flowerId: Int, success:@escaping(_ flowerDetail: FlowerDetail) -> Void)
    func fetchSpecificFlowers(query:String)
}

class HomeInteractor {
  var presenter: HomePresentationLogic?
  var getFlowersWorker    = GetFlowersWorker()
  var flowerDetailsWorker = FlowersDetailsWorker()
  var getSpecificFlowersWorker = GetSpecificFlowersWorker()
}

// MARK: - Business Logic
extension HomeInteractor: HomeBusinessLogic {
    func fetchSpecificFlowers(query: String) {
        getSpecificFlowersWorker.downloader.showLoadingSpinner()
        getSpecificFlowersWorker.execute(query: query, success: { (flowers) in
            self.getSpecificFlowersWorker.downloader.hideLoadingSpinner()
            self.presenter?.presentFlowers(flowers)
        }) { (error) in
            self.getSpecificFlowersWorker.downloader.hideLoadingSpinner()
            self.presenter?.presentFlowersError(error)
        }
        
    }

  func fetchFlowers() {
     flowerDetailsWorker.downloader.showLoadingSpinner()
    getFlowersWorker.execute(success: { (flowers) in
      self.flowerDetailsWorker.downloader.hideLoadingSpinner()
      self.presenter?.presentFlowers(flowers)
    }, failure: { error in
      self.flowerDetailsWorker.downloader.hideLoadingSpinner()
      self.presenter?.presentFlowersError(error)
    })
  }
  func fetchFlowerDetails(flowerId: Int,
                          success:@escaping(_ flowerDetail: FlowerDetail) -> Void) {
        flowerDetailsWorker.downloader.showLoadingSpinner()
    flowerDetailsWorker.execute(flowerId: flowerId, success: { [unowned self](flower) in
        self.flowerDetailsWorker.downloader.hideLoadingSpinner()
        success(flower)
    }, failure: { [unowned self] error in
        self.flowerDetailsWorker.downloader.hideLoadingSpinner()
        self.presenter?.presentFlowersError(error)
    })
  }
}
