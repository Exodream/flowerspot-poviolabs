//
//  HomeRouter.swift
//  FlowrSpot
//
//  Created by TK on 16/01/2018.
//  Copyright © 2018 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol HomeRoutingLogic {
  func navigateToFlowerDetails(flower: FlowerDetail)
}
protocol HomeRouterDelegate: class {
  
}
class HomeRouter {
  weak var viewController: HomeViewController?
  weak var delegate: HomeRouterDelegate?
}
// MARK: - Routing Logic
extension HomeRouter: HomeRoutingLogic {
  func navigateToFlowerDetails(flower: FlowerDetail) {
    let storyboard = UIStoryboard(name: UIStoryboard.Storyboard.main.filename, bundle: nil)
    let flowerDetailView = storyboard.instantiateViewController(withIdentifier: FlowerDetailView.storyboardIdentifier) as? FlowerDetailView
    flowerDetailView?.flowerDetail = flower
 viewController?.navigationController?.pushViewController(flowerDetailView!, animated: true)
  }
}
