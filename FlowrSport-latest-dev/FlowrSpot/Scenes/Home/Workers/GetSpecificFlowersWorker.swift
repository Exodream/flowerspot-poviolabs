//
//  GetSpecificFlowersWorker.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct GetSpecificFlowersWorker {

  var downloader = FlowersDownloader()
  
    func execute(query:String,
                 success: RestClient.SuccessCompletion<[Flower]>,
                 failure: RestClient.FailureCompletion) {
        
    downloader.fetchSpecificFlowers(query: query,
                                    success: { (flowers) in
      success?(FlowerResponseMapper.map(response: flowers))
    }, failure: failure)
  }
}
