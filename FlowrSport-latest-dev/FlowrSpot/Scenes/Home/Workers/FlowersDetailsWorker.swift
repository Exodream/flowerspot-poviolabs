//
//  FlowersDetailsWorker.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct FlowersDetailsWorker {
  
  var downloader = FlowersDownloader()
  
  func execute(flowerId: Int, success: RestClient.SuccessCompletion<FlowerDetail>, failure: RestClient.FailureCompletion) {
    downloader.fetchFlowerDetails(flowerId: flowerId, success: { (response) in
        success?(FlowerDetailResponseMapper.transform(response: response)!)
    }, failure: failure)
  }
}
