//
//  NewSightingView.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

class NewSightingView: UIViewController {

    private var router: SightingRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        router = SightingRouter()
        router?.viewController = self
        setupNavigationItems()
    }
    @objc func backItemPressed() {
        router?.navigateToFlowerDetail()
    }
}
extension NewSightingView: UIStylingViewController {
     func setupNavigationItems() {
        navigationItem.title = "general_app_name".localized()
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "backIcon"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 13, height: 21)
        backButton.addTarget(self, action: #selector(self.backItemPressed), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    func setupViews() {
        print("setup view here...")
    }
}
