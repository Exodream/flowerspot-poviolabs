//
//  SightingRouter.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol SightingRoutingLogic {
    func navigateToFlowerDetail()
}
class SightingRouter {
    weak var viewController: NewSightingView?
}
extension SightingRouter: SightingRoutingLogic {

    func navigateToFlowerDetail() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
