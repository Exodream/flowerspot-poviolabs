//
//  FlowerDetailDataSource.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

class FlowerDetailDataSource: DataSource {
    var sections = [FlowerDetailSection]()
    var flowerDetails = [Sighting]()

    func update(flowerSighting: [Sighting]) {
        self.flowerDetails = flowerSighting
        buildSections()
    }
}
private extension FlowerDetailDataSource {
    func buildSections() {
    let rows =
        flowerDetails.map(FlowerDetailRow.flowerDetail)
        sections = [FlowerDetailSection(rows: rows)]
    }
}
