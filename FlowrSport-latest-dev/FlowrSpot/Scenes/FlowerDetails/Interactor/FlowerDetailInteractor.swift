//
//  FlowerDetailInteractor.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol DetailBusinessLogic {
    func fetchSighting(flowerDetail: FlowerDetail)
}
class FlowerDetailInteractor {
    var presenter: DetailPresentationLogic?
    var getFlowerSighting = GetFlowerSighting()
}
extension FlowerDetailInteractor: DetailBusinessLogic {
    func fetchSighting(flowerDetail: FlowerDetail) {
        getFlowerSighting.downloader.showLoadingSpinner()
        getFlowerSighting.execute(flowerid: flowerDetail.id, success: { [unowned self](arrayFlowerSighting) in
            self.getFlowerSighting.downloader.hideLoadingSpinner()

            self.presenter?.presentFlowerSightings(arrayFlowerSighting)
            }, failure: { error in
                self.presenter?.presentFlowerSightingError(error)
        })
    }
}
