//
//  FlowerDetailPresenter.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

protocol DetailPresentationLogic {
    func presentFlowerSightings(_ sighting: [Sighting])
    func presentFlowerSightingError(_ error: RemoteResourceError)
    func presentFlowerDetail(_ detail: FlowerDetail)
}
class FlowerDetailPresenter {
     weak var viewController: DetailDisplayLogic?
}
// MARK: - Presentation Logic
extension FlowerDetailPresenter: DetailPresentationLogic {
    func presentFlowerSightings(_ sighting: [Sighting]) {
        viewController?.displayFlowerSightings(sighting)
    }
    func presentFlowerSightingError(_ error: RemoteResourceError) {
        viewController?.displayError(error)
    }
    func presentFlowerDetail(_ detail: FlowerDetail) {
        viewController?.displayFlowerDetail(detail)
    }
}
