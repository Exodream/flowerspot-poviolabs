//
//  GetFlowerSignhting.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation

struct GetFlowerSighting {
    var downloader = FlowersDownloader()

    func execute(flowerid: Int, success: RestClient.SuccessCompletion<[Sighting]>, failure: RestClient.FailureCompletion) {
        downloader.fetchFlowerSighting(flowerId: flowerid, success: { (flowerSighting) in
                if let sighting =
                    FlowerSightingResponseMapper.transform(response: flowerSighting) {
                success?(sighting)
                } else {
                    failure!(RemoteResourceError.invalidJson)
                }
        }, failure: failure)
    }
}
