//
//  FlowerDetailRouter.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit
import Foundation

protocol DetailRoutingLogic {
    func navigateToHomeView()
    func navigateToAddSightings()
}
class FlowerDetailRouter {
      weak var viewController: FlowerDetailView?
}
extension FlowerDetailRouter: DetailRoutingLogic {
    func navigateToAddSightings() {
        viewController?.performSegue(withIdentifier: "segAddSightings", sender: nil)
    }
    func navigateToHomeView() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
