//
//  FlowerSightingTableViewCell.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class FlowerSightingTableViewCell: UITableViewCell {

    @IBOutlet weak var imgvUserImage: UIImageView!
    @IBOutlet weak var lblFlowerName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblFlowerDescription: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblFavorites: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgvFlowerImage: UIImageView!
    @IBOutlet weak var viewLocation: UIView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Public methods
extension FlowerSightingTableViewCell {
    func setFlowerDetail(_ sighting: Sighting) {

        let userImagePath = "https://assets.change.org/photos/2/gd/gb/KuGDgbpHubFRjIR-800x450-noPad.jpg?1529419196"
        imgvUserImage.kf.setImage(with: URL(string: userImagePath))
        lblFlowerName.text = sighting.name
        lblUserName.text = "by " + sighting.user.fullName
        lblFlowerDescription.text = sighting.descriptionField
        lblComments.text = "comments_count".localized(args: sighting.commentsCount)
        lblFavorites.text = "favorites_count".localized(args: sighting.likesCount)
        if let latitude = sighting.latitude, let longitude = sighting.longitude {
        CLGeocoder.getAdress(latitude: Double(latitude), longitude: Double(longitude)) { [unowned self] (address, _) in
            guard address != nil, address?.isEmpty == false else {
                self.lblLocation.text = "unknown_location".localized()
                return
            }
            self.lblLocation.text = address
        }
        } else {
            self.lblLocation.text = "unknown_location".localized()
        }
        
        viewLocation.cornerRadius(radius: 10.0)
        imgvUserImage.setRounded()
        imgvFlowerImage.kf.setImage(with: URL(string: sighting.flower.url))
    }
}
