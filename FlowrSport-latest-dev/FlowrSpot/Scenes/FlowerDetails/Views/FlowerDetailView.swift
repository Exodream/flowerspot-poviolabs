//
//  FlowerDetailView.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol DetailDisplayLogic: class {
    func displayFlowerSightings(_ sightings: [Sighting])
    func displayError(_ error: RemoteResourceError)
    func displayFlowerDetail(_ detail: FlowerDetail)
}

class FlowerDetailView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddSighting: UIButton!
    @IBOutlet weak var headerView: FlowerHeaderView!
    
    var flowerDetail: FlowerDetail!
    var interactor: FlowerDetailInteractor?
    private var router: FlowerDetailRouter?
    private var presenter: FlowerDetailPresenter!
    private let flowerDetailDataSource = FlowerDetailDataSource()

    func setupProtocols() {
        let interactor = FlowerDetailInteractor()
        presenter      = FlowerDetailPresenter()
        let router     = FlowerDetailRouter()
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController    = self
        self.interactor = interactor
        self.router     = router
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProtocols()
        setupViews()
        makeRoundedViews()
        setupNavigationItems()
        presenter.presentFlowerDetail(flowerDetail)
        interactor?.fetchSighting(flowerDetail: flowerDetail)
    }
    @IBAction func actionAddSighting() {
        router?.navigateToAddSightings()
    }
    @objc func backItemPressed() {
        router?.navigateToHomeView()
    }
}

extension FlowerDetailView: DetailDisplayLogic {
    func displayFlowerSightings(_ sightings: [Sighting]) {
        flowerDetailDataSource.update(flowerSighting: sightings)
        tableView.reloadData()
    }
    func displayError(_ error: RemoteResourceError) {
        let alert = UIAlertController(title: "general_error".localized(), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "general_ok".localized(), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func displayFlowerDetail(_ detail: FlowerDetail) {
        headerView.presentData(detail: detail)
        calculateHeight()
    }
}

// MARK: - UITableViewDataSource
extension FlowerDetailView: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return flowerDetailDataSource.numberOfSections()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flowerDetailDataSource.numberOfRows(in: section)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let row = flowerDetailDataSource.row(at: indexPath) else {
             Logger.error("No availible row in dataSource at \(indexPath)")
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(FlowerSightingTableViewCell.self, at: indexPath)

        switch row {
        case let .flowerDetail(entity):
                cell.setFlowerDetail(entity)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
}
// MARK: - UIScroolViewDelegate
extension FlowerDetailView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPoint.zero
        }
    }
}
// MARK: - StylingViewController
extension FlowerDetailView: UIStylingViewController {
    func setupViews() {
        headerView.setupViews()
        tableView.accessibilityIdentifier = "detailTable"
        tableView.tableFooterView = UIView()
    }
    func setupNavigationItems() {
        navigationItem.title = "general_app_name".localized()
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "backIcon"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 13, height: 21)
        backButton.addTarget(self, action: #selector(self.backItemPressed), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
}
extension FlowerDetailView {
    func calculateHeight() {
        let heightOfFeaturesView    = headerView.calculateFeatures()
        let heightOfDescriptionView = headerView.calculateDescription()
        let constraintHeight: CGFloat = 34
        headerView.frame = CGRect(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: headerView.frame.size.width, height: 430 + heightOfDescriptionView + constraintHeight + heightOfFeaturesView)
    }
     func makeRoundedViews() {
        headerView.makeRoundedViews()
        btnAddSighting.cornerRadius(radius: 2)
    }
}
// MARK: FormatString
private protocol StringFormatter {
    func appendNewLinesOnStringArray(stringArray: [String]) -> String
}
extension FlowerHeaderView: StringFormatter {
    func appendNewLinesOnStringArray(stringArray: [String]) -> String {
        var newString = ""
        for string in stringArray {
            newString += string + "\n\n"
        }
        if newString.count > 2 {
            newString.removeLast()
            newString.removeLast()
        }
       
        return newString
    }
}
extension FlowerDetailView: StoryboardIdentifiable { }
extension FlowerSightingTableViewCell: TableViewCellIdentifiable { }
