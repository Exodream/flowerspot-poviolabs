//
//  NavigationViewController.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupConstraints()
  }
}

// MARK: - UIStyling
extension NavigationViewController: UIStyling {
  func setupViews() {
    navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.custom(type: .bold, size: 17),
                                         NSAttributedString.Key.foregroundColor: UIColor.flowrPink]
    navigationBar.setBackgroundImage(UIImage(), for: .top, barMetrics: .default)
    navigationBar.shadowImage = UIImage()
    navigationBar.layer.masksToBounds = false
    navigationBar.barTintColor = UIColor.navigationBackground
    navigationBar.isTranslucent = false
    navigationItem.leftBarButtonItem?.tintColor = UIColor.yellow
    self.navigationBar.tintColor = .red
  }
  
  func setupConstraints() {
    
  }
}
