//
//  FlowerHeaderView.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol PresentViewData {
    func presentData(detail: FlowerDetail)
    func setupViews()
    func calculateDescription() -> CGFloat
    func calculateFeatures() -> CGFloat
    func makeRoundedViews()
}
protocol SizeableTextHeight {
    func calculateHeightOfText() -> CGFloat
}

class FlowerHeaderView: UIView {
    @IBOutlet weak var lblFlowerName: UILabel!
    @IBOutlet weak var lblLatinFlowerName: UILabel!
    @IBOutlet weak var imgvFlower: UIImageView!
    @IBOutlet weak var lblSightingCount: UILabel!
    @IBOutlet weak var lblFeatures: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var imgvFavoriteStar: UIImageView!
    @IBOutlet weak var containerSighting: UIView!
    @IBOutlet weak var viewStar: UIView!
}
extension FlowerHeaderView: PresentViewData {
    func presentData(detail: FlowerDetail) {
        lblFlowerName.text = detail.name
        lblLatinFlowerName.text = detail.latinName
        imgvFlower.kf.setImage(with: URL(string: detail.url))
        lblSightingCount.text = "sightings_count".localized(args: detail.sightings)
        lblFeatures.font = UIFont.custom(type: .bold, size: 13.0)
        lblFeatures.text = ""
        lblFeatures.text = appendNewLinesOnStringArray(stringArray: detail.features!)
        txtDescription.text = detail.description
        imgvFavoriteStar.image = detail.isFavorite == true ? UIImage(named: "pl-icon-star-ON") : UIImage(named: "pl-icon-star-OFF")
    }
    func setupViews() {
        txtDescription.textContainerInset = UIEdgeInsets.zero
        txtDescription.textContainer.lineFragmentPadding = 0
    }
    func calculateFeatures() -> CGFloat {
        return lblFeatures.calculateHeightOfText()
    }
    func calculateDescription() -> CGFloat {
        return txtDescription.calculateHeightOfText()
    }
    func makeRoundedViews() {
        viewStar.setRounded()
        containerSighting.cornerRadius(radius: 15)
    }
}
extension UIView: SizeableTextHeight {
    func calculateHeightOfText() -> CGFloat {
        let sizeThatFitsTextView = sizeThatFits(CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT)))
        return sizeThatFitsTextView.height
    }
}
