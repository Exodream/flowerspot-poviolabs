//
//  UIVIew+Extra.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

extension UIView {
  class func autolayoutView() -> Self {
    let instance = self.init()
    instance.translatesAutoresizingMaskIntoConstraints = false
    return instance
  }
  
  func autoLayoutView() -> Self {
    translatesAutoresizingMaskIntoConstraints = false
    return self
  }
}
