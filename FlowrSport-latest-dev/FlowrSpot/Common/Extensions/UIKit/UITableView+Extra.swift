//
//  UITableView+Extra.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(_ type: T.Type) {
        register(type, forCellReuseIdentifier: type.reuseIdentifier)
    }
    func dequeueReusableCell<T: UITableViewCell>(_ cell: T.Type, at indexPath: IndexPath) -> T {
        guard let cell =
            dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            Logger.error("Could not dequeue cell with identifier: \(T.reuseIdentifier). Creating new instance.")
            return T()
        }
        return cell
    }
}

