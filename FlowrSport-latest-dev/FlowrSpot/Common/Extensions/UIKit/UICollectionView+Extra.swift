//
//  UICollectionView+Extra.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

extension UICollectionView {
  func register<T: UICollectionViewCell>(_ type: T.Type) {
    register(type, forCellWithReuseIdentifier: type.reuseIdentifier)
  }
  
  func dequeueReusableCell<T: UICollectionViewCell>(_ cell: T.Type, at indexPath: IndexPath) -> T {
    guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      Logger.error("Could not dequeue cell with identifier: \(T.reuseIdentifier). Creating new instance.")
      return T()
    }
    return cell
  }
}

