//
//  CLGeocoder+Extra.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import CoreLocation

extension CLGeocoder {

static func getAdress(latitude: Double, longitude: Double, completion: @escaping (String?, Error?) -> Void) {
    CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
        guard let placemark = placemarks?.first, error == nil else {
            completion(nil, error)
            return
        }
        var address = ""
        if let city = placemark.locality {
            address += city + ", "
        }
        if let country = placemark.isoCountryCode {
            address += country
        }
        completion(address, nil)
    }
  }
}
