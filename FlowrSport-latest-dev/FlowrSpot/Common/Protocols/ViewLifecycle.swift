//
//  ViewLifecycle.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol ViewLifecycle {
  func setupViews()
  func setupConstraints()
}

extension ViewLifecycle where Self: UIView {
  func setup() {
    backgroundColor = .white
    setupViews()
    setupConstraints()
  }
}
