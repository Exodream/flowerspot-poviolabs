//
//  UIStyling.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
import UIKit

protocol UIStyling {
  func setupViews()
  func setupConstraints()
}
