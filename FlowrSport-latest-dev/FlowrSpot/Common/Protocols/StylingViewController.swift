//
//  StylingViewController.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol UIStylingViewController {
    func setupNavigationItems()
    func setupViews()
}
extension UIStylingViewController where Self: UIViewController { }
