//
//  StylingRound.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol StylingRound {
    func setRounded()
    func cornerRadius(radius: CGFloat)
}
extension StylingRound where Self: UIView {
    func setRounded() {
        self.layer.cornerRadius = frame.height / 2
        self.layer.masksToBounds = true
    }
    func cornerRadius(radius: CGFloat) {
        layer.cornerRadius = radius
    }
}
extension UIView: StylingRound { }
