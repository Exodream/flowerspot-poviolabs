//
//  ItemDimension.swift
//  FlowrSpot
//
//  Created by Milos Stevanovic on 4/12/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import UIKit

protocol ItemDimension {
  func sizeForItem(at indexPath: IndexPath) -> CGSize
}

protocol CollectionViewItemDimension {
  func sizeForItem(at indexPath: IndexPath, for collectionView: UICollectionView) -> CGSize
  
  var sectionInset: UIEdgeInsets { get }
  var lineSpacing: CGFloat { get }
  var interItemSpacing: CGFloat { get }
}
