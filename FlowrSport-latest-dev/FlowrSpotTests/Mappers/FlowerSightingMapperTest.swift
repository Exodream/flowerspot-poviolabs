//
//  FlowerSightingMapperTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowerSightingMapperTest: XCTestCase {
    private let response = FlowerSightingMock().mockFlowerSightingResponses()!

    func testMapping() {
        let entities = FlowerSightingResponseMapper.transform(response: response)
        XCTAssertNotNil(entities)

        for i in 0..<entities!.count {
            let sighting = entities![i]
            let _response = response[i]
            XCTAssertEqual(sighting.id, _response.id)
            XCTAssertEqual(sighting.name, _response.name)
            XCTAssertEqual(sighting.commentsCount, _response.commentsCount)
            XCTAssertEqual(sighting.createdAt, _response.createdAt)
            XCTAssertEqual(sighting.descriptionField, _response.descriptionField)
            XCTAssertEqual(sighting.picture, _response.picture)
            XCTAssertEqual(sighting.latitude, _response.latitude)
            XCTAssertEqual(sighting.likesCount, _response.likesCount)
            XCTAssertEqual(sighting.longitude, _response.longitude)
            XCTAssertEqual(sighting.user.id, _response.user?.id)
            XCTAssertEqual(sighting.flower.id, _response.flower?.id)
        }
    }
}
