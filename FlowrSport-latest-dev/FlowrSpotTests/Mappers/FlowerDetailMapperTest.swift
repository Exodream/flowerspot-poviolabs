//
//  FlowerDetailMapperTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowerDetailMapperTest: XCTestCase {
    private let response = FlowerDetailMock().mockFlowerDetailResponses()!

    func testMapping() {
        let entity = FlowerDetailResponseMapper.transform(response: response)
        XCTAssertNotNil(entity)
        XCTAssertEqual(entity?.name, response.name)
        XCTAssertEqual(entity?.latinName, response.latinName)
        XCTAssertEqual(entity?.sightings, response.sightings)
        XCTAssertEqual(entity?.isFavorite, response.isFavorite)
        XCTAssertEqual(entity?.url, "http:" + response.profilePicture)
    }
}

