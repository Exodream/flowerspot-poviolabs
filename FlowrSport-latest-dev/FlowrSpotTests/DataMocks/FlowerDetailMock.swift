//
//  FlowerDetailMock.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
@testable import FlowrSpot

class FlowerDetailMock {
    func mockFlowerDetailEntitiy() -> FlowerDetail? {
        if let mockDetailResponse = mockFlowerDetailResponses() {
            return FlowerDetailResponseMapper.transform(response: mockDetailResponse)
        }
        return nil
    }
    func mockFlowerDetailResponses() -> FlowerDetailsResponse? {
        do {
            let json = try laodJsonFromFile("flowerDetail")
            return FlowerDetailsResponse.parse(json, key:"flower")!
        }
        catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
private extension FlowerDetailMock {
    func laodJsonFromFile(_ file: String) throws -> Any {
        guard let path = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else { throw RemoteResourceError.generic }

        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        return jsonResult
    }
}
