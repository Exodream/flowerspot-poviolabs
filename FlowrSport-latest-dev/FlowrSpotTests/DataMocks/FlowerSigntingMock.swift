//
//  FlowerSigntingMock.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import Foundation
@testable import FlowrSpot

class FlowerSightingMock {
    func mockFlowerDetailEntitiy() -> [Sighting]? {
        if let mockSightingResponse = mockFlowerSightingResponses(){
          return  FlowerSightingResponseMapper.transform(response: mockSightingResponse)
        }
        return nil
    }
    func mockFlowerSightingResponses() -> [SightingResponse]? {
        do {
            let json = try laodJsonFromFile("flowerSighting")
            return   FlowerSightingResponse.parse(json, key: "sightings")?.sightings
        }
        catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
private extension FlowerSightingMock {
    func laodJsonFromFile(_ file: String) throws -> Any {
        guard let path = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else { throw RemoteResourceError.generic }

        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        return jsonResult
    }
}
