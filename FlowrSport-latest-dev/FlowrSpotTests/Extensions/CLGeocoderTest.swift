//
//  CLGeocoderTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import CoreLocation
import XCTest
@testable import FlowrSpot

class CLGeocoderTest: XCTestCase {

    /// Belgrade latitude and longitude
    func testLocation(){
        let latitude: Double   = 44.7866
        let longitude: Double  = 20.4489
        let promise = expectation(description: "Completion handler invoked")
        CLGeocoder.getAdress(latitude: latitude, longitude: longitude) { (adress, error) in
            XCTAssertNil(error)
            print(adress!)
            promise.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
}
