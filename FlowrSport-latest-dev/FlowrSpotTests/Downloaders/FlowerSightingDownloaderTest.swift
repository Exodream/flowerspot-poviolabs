//
//  FlowerSightingDownloaderTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowerSightingDownloaderTest: XCTestCase {
    private let flowersDownloader = FlowersDownloader()
    private let flowerDetail = FlowerDetailMock().mockFlowerDetailEntitiy()!
    private var flowerSightings = FlowerSightingMock().mockFlowerDetailEntitiy()!
    private var fetchSightings : [Sighting]!

    func testFetchFlowerSightings() {
        let promise = expectation(description: "Completion handler invoked")
        flowersDownloader.fetchFlowerSighting(flowerId: flowerDetail.id, success: { [unowned self](sightingResponse) in
            XCTAssertNotNil(sightingResponse)
            if let sightings  = FlowerSightingResponseMapper.transform(response: sightingResponse){
                self.fetchSightings = sightings
            }
            promise.fulfill()
        }, failure: {error in
            XCTFail("Request should succeed" + error.localizedDescription)
            promise.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
        findeEntity()
    }
    private func findeEntity(){
        let compareArray = zip(fetchSightings, flowerSightings).enumerated().filter{$1.0.id == $1.1.id}.map{$0.0}
        XCTAssertGreaterThan(compareArray.count, 0)
    }
}
