//
//  FlowerDetailDownloaderTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowerDetailDownloaderTest: XCTestCase {
    private let flowersDownloader = FlowersDownloader()
    private let flowerDetail = FlowerDetailMock().mockFlowerDetailEntitiy()!
    private var fetchDetail : FlowerDetail!

    func testFetchFlowerDetail() {
        let promise = expectation(description: "Completion handler invoked")
        flowersDownloader.fetchFlowerDetails(flowerId: flowerDetail.id, success: { [unowned self] (flowerDetailResponse) in
            XCTAssertNotNil(flowerDetailResponse)
            if  let fetchDetail = FlowerDetailResponseMapper.transform(response: flowerDetailResponse) {
                self.fetchDetail = fetchDetail
            }
            promise.fulfill()
        }, failure: { error in
            XCTFail("Request should succeed" + error.localizedDescription)
            promise.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
        compareEntity()
    }
    private func compareEntity(){
        XCTAssertNotNil(fetchDetail)
        XCTAssertEqual(fetchDetail.latinName, flowerDetail.latinName)
        XCTAssertEqual(fetchDetail.name, flowerDetail.name)
        XCTAssertEqual(fetchDetail.description, flowerDetail.description)
        XCTAssertEqual(fetchDetail.url, flowerDetail.url)
        XCTAssertEqual(fetchDetail.id, flowerDetail.id)
        XCTAssertEqual(fetchDetail.isFavorite, flowerDetail.isFavorite)
        XCTAssertEqual(fetchDetail.sightings, flowerDetail.sightings)
        let countFeatures = zip(fetchDetail.features!, flowerDetail.features!).enumerated().filter{
            $1.0 == $1.1}
        XCTAssertEqual(countFeatures.count, flowerDetail.features!.count)
    }
}
