//
//  FlowerDetailSourceTest.swift
//  FlowrSpotTests
//
//  Created by Milos Stevanovic on 4/20/20.
//  Copyright © 2020 PovioLabs. All rights reserved.
//

import XCTest
@testable import FlowrSpot

class FlowerDetailSourceTests: XCTestCase {
     private let sightings = FlowerSightingMock().mockFlowerDetailEntitiy()
     private let dataSource = FlowerDetailDataSource()

    func testDataSource() {
        XCTAssertNotNil(sightings)
        dataSource.update(flowerSighting: sightings!)

        XCTAssertTrue(dataSource.sections.count == 1)
        XCTAssertTrue(dataSource.sections.first?.rows.count == sightings!.count)

        for i in 0..<sightings!.count {
            let row = dataSource.row(at: IndexPath(row: i, section: 0))
            XCTAssertNotNil(row)
            
            if let row = row {
                switch row {
                case let .flowerDetail(entity):
                    XCTAssertEqual(sightings![i].name, entity.name)
                }
            }
        }
    }
}
